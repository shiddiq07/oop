<?php
require ('animal.php');
$hewan = new Animal("shaun");

echo "Name :".$hewan->nama."<br>";
echo "legs :".$hewan->legs."<br>";
echo "cold blooded :".$hewan->cold_blooded."<br><br><br>";

require ('ape.php');
$kera = new Ape("kera sakti");

echo "Name :".$kera->nama."<br>";
echo "legs :".$kera->legs."<br>";
echo "cold blooded :".$kera->cold_blooded."<br>";
echo "Yell : ".$kera->jump."<br><br><br>";


require ('frog.php');
$kodok = new Frog("buduk");

echo "Name :".$kodok->nama."<br>";
echo "legs :".$kodok->legs."<br>";
echo "cold blooded :".$kodok->cold_blooded."<br>";
echo "Jump : ".$kodok->jump."<br><br><br>";

?>
